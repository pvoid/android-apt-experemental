/*
 * Copyright 2016 Dmitrii Petukhov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.pvoid.androidapt

import org.gradle.api.Project
import org.gradle.api.internal.plugins.PluginApplicationException
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class AndroidAptPluginTest {

    def VERSION = "1.0.1"

    @Rule
    public final TemporaryFolder mProjectFolder = new TemporaryFolder();
    private File mBuildFile;

    @Before
    public void setup() {
        mBuildFile = mProjectFolder.newFile("build.gradle");

        mProjectFolder.newFolder("src", "main")
        def manifestFile = mProjectFolder.newFile("src/main/AndroidManifest.xml");
        writeFile(manifestFile,"""
<manifest package="org.bitbucket.pvoid.androidapt"
          xmlns:android="http://schemas.android.com/apk/res/android">
    <application
        android:name=".TestApp"
        android:label="Test App"/>
</manifest>
"""
);

        mProjectFolder.newFolder("src", "main", "java", "org", "bitbucket", "pvoid", "androidapt");
        def sourceFile = mProjectFolder.newFile("src/main/java/org/bitbucket/pvoid/androidapt/TestApp.java");
        writeFile(sourceFile,"""
package org.bitbucket.pvoid.androidapt;

import android.app.Application;
import org.bitbucket.pvoid.test.CheckAnnotation;

@CheckAnnotation
public class TestApp extends Application {
}
""");
    }

    @Test(expected = PluginApplicationException.class)
    public void testThrowOnNonAndroidProject() {
        Project project = ProjectBuilder.builder().build();
        project.apply plugin: 'org.bitbucket.pvoid.android-apt'

        AndroidAptPluginExperimental.Rules.apply(project)
    }

    @Test
    public void testNonFlavourBuild() {
        writeFile(mBuildFile,"""
buildscript {
    repositories {
        jcenter()
        mavenLocal()
    }

    dependencies {
        classpath 'com.android.tools.build:gradle-experimental:0.6.0-beta6'
        classpath 'org.bitbucket.pvoid:android-apt-experemental:$VERSION'
    }
}

repositories {
    jcenter()
    mavenLocal()
}

apply plugin: 'com.android.model.application'
apply plugin: 'org.bitbucket.pvoid.android-apt'

model {
    android {
        compileSdkVersion 23
        buildToolsVersion "23.0.2"

        defaultConfig {
            applicationId "org.bitbucket.pvoid.androidapt"
            minSdkVersion.apiLevel 14
            targetSdkVersion.apiLevel 23
            versionCode 1
            versionName "1.0"
        }
    }

    apt {
        params {
            create() {
                name "name1"
                value "Value 1"
            }

            create() {
                name "name2"
                value "Value 2"
            }
        }
    }
}

dependencies {
    apt 'org.bitbucket.pvoid:annotation-processor:$VERSION'
    compile 'org.bitbucket.pvoid:test-annotations:$VERSION'
}
""");

        BuildResult result = GradleRunner.create()
                .withProjectDir(mProjectFolder.getRoot())
                .withArguments("assembleDebug")
                .build();

        Assert.assertEquals(TaskOutcome.SUCCESS, result.task(":assembleDebug").getOutcome());
        // check that build produced output file
        def apkPath = new File(mProjectFolder.getRoot(), "build/outputs/apk/")
        def apkFile = new File(apkPath, mProjectFolder.getRoot().getName() + "-debug.apk");
        Assert.assertTrue(apkFile.exists());

        // check params
        def sourceFile = new File(mProjectFolder.getRoot(), "build/generated/source/apt/main/org/bitbucket/pvoid/test/Params.java")
        Assert.assertTrue(sourceFile.exists());
        Map<String, String> params = readParams(sourceFile);
        Assert.assertEquals(12, params.size());
        Assert.assertEquals("'Value 1'", params.get("name1"));
        Assert.assertEquals("'Value 2'", params.get("name2"));
        Assert.assertEquals("'Debug'", params.get("buildType"));
        Assert.assertEquals("'Debug'", params.get("buildType"));
    }

    @Test
    public void testFlavouredBuild() {
        writeFile(mBuildFile, """
buildscript {
    repositories {
        jcenter()
        mavenLocal()
    }

    dependencies {
        classpath 'com.android.tools.build:gradle-experimental:0.6.0-beta6'
        classpath 'org.bitbucket.pvoid:android-apt-experemental:$VERSION'
    }
}

repositories {
    jcenter()
    mavenLocal()
}

apply plugin: 'com.android.model.application'
apply plugin: 'org.bitbucket.pvoid.android-apt'

model {
    android {
        compileSdkVersion 23
        buildToolsVersion "23.0.2"

        defaultConfig {
            applicationId "org.bitbucket.pvoid.androidapt"
            minSdkVersion.apiLevel 14
            targetSdkVersion.apiLevel 23
            versionCode 1
            versionName "1.0"
        }

        productFlavors {
            create("flavor1") {
            }
            create("flavor2") {
            }
        }
    }

    apt {
        params {
            create() {
                name "name1"
                value "Value 1"
            }

            create() {
                name "name2"
                value "Value 2"
            }
        }
    }
}

dependencies {
    apt 'org.bitbucket.pvoid:annotation-processor:$VERSION'
    compile 'org.bitbucket.pvoid:test-annotations:$VERSION'
}
""");

        BuildResult result = GradleRunner.create()
                .withProjectDir(mProjectFolder.getRoot())
                .withArguments("assembleDebug")
                .build();

        Assert.assertEquals(TaskOutcome.SUCCESS, result.task(":assembleDebug").getOutcome());
        // check that build produced output file
        def apkPath = new File(mProjectFolder.getRoot(), "build/outputs/apk/")
        Assert.assertTrue((new File(apkPath, mProjectFolder.getRoot().getName() + "-flavor1-debug.apk")).exists());
        Assert.assertTrue((new File(apkPath, mProjectFolder.getRoot().getName() + "-flavor2-debug.apk")).exists());

        // check params for first flavor
        def sourceFile = new File(mProjectFolder.getRoot(), "build/generated/source/apt/flavor1/org/bitbucket/pvoid/test/Params.java")
        Assert.assertTrue(sourceFile.exists());
        Map<String, String> params = readParams(sourceFile);
        Assert.assertEquals(12, params.size());
        Assert.assertEquals("'Value 1'", params.get("name1"));
        Assert.assertEquals("'Value 2'", params.get("name2"));
        Assert.assertEquals("'Debug'", params.get("buildType"));

        // check params for second flavor
        sourceFile = new File(mProjectFolder.getRoot(), "build/generated/source/apt/flavor2/org/bitbucket/pvoid/test/Params.java")
        Assert.assertTrue(sourceFile.exists());
        params = readParams(sourceFile);
        Assert.assertEquals(12, params.size());
        Assert.assertEquals("'Value 1'", params.get("name1"));
        Assert.assertEquals("'Value 2'", params.get("name2"));
        Assert.assertEquals("'Debug'", params.get("buildType"));
    }

    private static Map<String, String> readParams(File file) {
        Map<String, String> map = new HashMap<>();

        Reader reader = new FileReader(file);
        List<String> lines = reader.readLines();
        Iterator<String> it = lines.iterator();
        Assert.assertEquals("package org.bitbucket.pvoid.test;", it.next());
        Assert.assertEquals("", it.next());
        Assert.assertEquals("public class Params {", it.next());
        String line;
        while (it.hasNext() && !"}".equals(line = it.next().trim())) {
            Assert.assertTrue(line.startsWith("final static String "))
            line = line.substring("final static String ".length());
            def vals = line.split(" = ");
            def key = vals[0];
            def value = vals[1];
            if (value.length() > 0) {
                value = value.substring(1, value.length() - 2);
            }
            map.put(key, value);
        }
        return map;
    }

    private static void writeFile(File destination, String content) throws IOException {
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new FileWriter(destination));
            output.write(content);
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }
}
