/*
 * Copyright 2016 Dmitrii Petukhov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.pvoid.androidapt

import com.android.build.gradle.managed.ProductFlavor
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.ProjectConfigurationException
import org.gradle.api.Task
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.compile.CompileOptions
import org.gradle.language.base.FunctionalSourceSet
import org.gradle.language.base.LanguageSourceSet
import org.gradle.model.Model
import org.gradle.model.ModelMap
import org.gradle.model.ModelSet
import org.gradle.model.Mutate
import org.gradle.model.Path
import org.gradle.model.RuleSource

class AndroidAptPluginExperimental implements Plugin<Project> {

    private static final String SOURCE_PATH = "generated/source/apt";

    @Override
    void apply(Project project) {
        if (!project.plugins.findPlugin("com.android.model.application") &&
            !project.plugins.findPlugin("com.android.model.library")) {
            throw new ProjectConfigurationException("The 'com.android.model.application' or 'com.android.model.application' plugin must be applied to the project", null)
        }
        project.configurations.create('apt')
    }

    public static class Rules extends RuleSource {
        @Model("apt")
        public void aptModel(AptModel model) {
        }

        @Mutate
        public void aptSources(@Path("android.sources") final ModelMap<FunctionalSourceSet> sources,
                               @Path("android.productFlavors") final ModelMap<ProductFlavor> flavors,
                               @Path("buildDir") final File buildPath) {
            sources.afterEach(new Action<FunctionalSourceSet>() {
                @Override
                public void execute(final FunctionalSourceSet functionalSourceSet) {
                    final String nodeName = functionalSourceSet.getProperties()["backingNode"];
                    String[] names = nodeName.split("\\.");
                    final sourceSetName = names[names.length - 1];

                    if (flavors.isEmpty() && sourceSetName.equals("main") || flavors.get(sourceSetName) != null) {
                        functionalSourceSet.afterEach(
                                new Action<LanguageSourceSet>() {
                                    @Override
                                    public void execute(LanguageSourceSet languageSourceSet) {
                                        // remember about jni
                                        if (!"java".equals(languageSourceSet.getName())) {
                                            return;
                                        }

                                        SourceDirectorySet source = languageSourceSet.getSource();
                                        File aptSources = new File(buildPath, SOURCE_PATH + "/" + sourceSetName);
                                        source.srcDir(aptSources);
                                    }
                                });
                    }
                }
            });
        }

        @Mutate
        public void aptOptions(ModelMap<Task> tasks,
                               @Path("buildDir") File buildPath,
                               @Path("android.productFlavors") final ModelMap<ProductFlavor> flavors,
                               @Path("android.sources") final ModelMap<FunctionalSourceSet> sources,
                               @Path("apt.params") ModelSet<AptParam> aptParams,
                               Project project) {
            def aptConfig = project.configurations.apt
            if (aptConfig.empty) {
                return
            }

            tasks.values().each { task ->
                if (task.name.startsWith("compile") && task.name.endsWith("JavaWithJavac")) {
                    task.doFirst {
                        def currentFlavor = task.name.substring(7, task.name.length() - 13);
                        def sourceSetName = "";

                        // check build type
                        def isRelease = false;
                        if (currentFlavor.endsWith("Debug")) {
                            currentFlavor -= "Debug";
                        } else if (currentFlavor.endsWith("Release")) {
                            currentFlavor -= "Release";
                        } else {
                            // unknown build type, just skip by now
                            return;
                        }

                        if (!flavors.isEmpty()) {
                            def flavor = flavors.find { flavor ->
                                def name = flavor.name.capitalize();
                                // by now we support only debug and release builds
                                return currentFlavor.equals(name)
                            }
                            if (flavor == null) {
                                return
                            }
                            sourceSetName = flavor.name;
                        } else if (currentFlavor.isEmpty()) {
                            sourceSetName = "main"
                        } else {
                            return;
                        }

                        def aptSources = new File(buildPath, SOURCE_PATH + "/" + sourceSetName);
                        task.source = task.source.findAll { file -> !file.absolutePath.startsWith(aptSources.absolutePath) }

                        CompileOptions options = task.options;

                        def processorPath = (aptConfig + task.classpath).asPath
                        options.compilerArgs += [
                            '-processorpath', processorPath
                        ]

                        options.compilerArgs += [
                            '-s', aptSources
                        ]


                        options.compilerArgs += ["-AbuildType=\'${isRelease ? "Release" : "Debug"}\'"]
                        def sourceSet = sources.get(sourceSetName)
                        if (sourceSet != null) {
                            sourceSet.each { src ->
                                options.compilerArgs += ["-Asource${src.name.capitalize()}=${src.source.srcDirs.join(';')}"]
                            }
                        }

                        aptParams.each {param ->
                            options.compilerArgs += [
                                "-A" + param.name + "=\'" + param.value + "\'"
                            ]
                        }

                        if (!aptSources.exists()) {
                            aptSources.mkdirs();
                        }
                    }
                }
            }
        }
    }
}
