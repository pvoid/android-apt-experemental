### Summary ###

It is a Gradle plugin which allows you to add annotation processor as compile time dependency without including them
in final artifact. It has been created for and supports only experimental android gradle plugin and Rule based model
for Gradle build configuration.

### Requirements ###

This plugin requires `com.android.model.application` or `com.android.model.library` to be configured on your project

### How to use it? ###

Add dependency from this plugin to your build script.

````
buildscript {
    repositories {
        jcenter()
    }

    dependencies {
        classpath 'com.android.tools.build:gradle-experimental:0.6.0-beta6'
        classpath 'org.bitbucket.pvoid:android-apt-experemental:1.0.1'
    }
}
````

Now you can add external dependencies as annotation processors in your application's `build.gradle` file

````
apply plugin: 'org.bitbucket.pvoid.android-apt'


compile 'com.google.dagger:dagger:2.0.2'
apt 'com.google.dagger:dagger-compiler:2.0.2'
````

### How to use latest version from repository? ###

Checkout latest version

````
git clone https://bitbucket.org/pvoid/android-apt-experemental.git
````

Build it and upload to local maven repo

````
cd android-apt-experemental
./gradlew android-apt:publishToMavenLocal
````

Add local maven as repository for your buildscript's dependencies. For that you should find `buildscript` section in your root `build.gradle`
file and add `mavenLocal()` in to it, so it should looks like

````
repositories {
    jcenter()
    mavenLocal()
}
````

Now you can add buildscript's dependency and configure annotation processors in the same way as in previous section

### Contribution ###

Android gradle plugin for new Gradle model is new and experimental. Anything can be broken at any time, so any
type of contribution is welcome. If something doesn't work for you, try at least open an issue with complete
description of the problem.