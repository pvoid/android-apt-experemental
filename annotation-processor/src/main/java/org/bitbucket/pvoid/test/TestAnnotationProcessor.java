/*
 * Copyright 2016 Dmitrii Petukhov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.pvoid.test;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.Set;

@SupportedAnnotationTypes({"org.bitbucket.pvoid.test.CheckAnnotation"})
public class TestAnnotationProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (roundEnv.getElementsAnnotatedWith(CheckAnnotation.class).isEmpty()) {
            return false;
        }

        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "Note from processor: It works!");

        try {
            JavaFileObject file = processingEnv.getFiler().createSourceFile("org.bitbucket.pvoid.test.Params");
            Writer writer = file.openWriter();

            Map<String, String> options = processingEnv.getOptions();
            writer.write("package org.bitbucket.pvoid.test;\n");
            writer.write("\n");
            writer.write("public class Params {\n");
            if (options != null) {
                for (Map.Entry<String, String> entry : options.entrySet()) {
                    writer.write("    final static String ");
                    writer.write(entry.getKey());
                    writer.write(" = \"");
                    writer.write(entry.getValue());
                    writer.write("\";\n");
                }
            }
            writer.write("}\n");

            writer.close();
        } catch (IOException e) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Can't create file: " + e.getMessage());
        }
        return true;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return processingEnv.getSourceVersion();
    }
}
